import spacy
from collections import namedtuple
from dataclasses import dataclass
from datastructures import Token, Article


def create_parser():
    return spacy.load("fr_core_news_sm")


def analyse_article(parser, article: Article) -> Article:
    result = parser((article.titre or "" ) + "\n" + (article.description or ""))
    output = []
    for token in result:
        # À effacer : stopwords, ponctuations, espaces, 
        # mots numériques (en chiffre et en latin), mots grammaticaux
        if (not token.is_punct) and (not token.is_space) and (not token.is_stop) and (not token.is_digit) and (token.pos_ not in ["ADP", "AUX", "CCONJ", "DET", "PRON", "SCONJ", "X"]):   
            output.append(Token(token.text, token.lemma_, token.pos_))
            # Résultat : dans les fichiers XML, les balises <analyse>
            # donne des listes de tokens qui NE LES CONTIENNENT PAS
    article.analyse = output
    return article

