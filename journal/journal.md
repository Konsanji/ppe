## Journal de bord
> Prénom NOM : Ji AN  
> Contact : ji.an@sorbonne-nouvelle.fr  
> Université : Paris Nanterre  

---

### 22/02/2023
**Départ du projet**
- déposer 10 fichiers anglais dans le dossier `./Corpus`
- ajouter `extraire_lexique.py` 
- créer les branches `JA-s2`, `page` et ce `journal de bord`.
  
---

### 17/05/2023
**Réorganisation du projet**
- supprimer les branches qui ne sont plus à jour / utiles pour le travail personnel (`page` & `JA-s2`)
- ajouter les dossiers `html`, `journal`, `Scripts`, `Output` & `exercices`

--- 

### 18/05/2023
**Reprise du projet**
- chercher un template html sur Internet
- tester les scripts sur plusieurs catégories telles que `idées`, `cinéma`, `voyage`
- exporter les corpus dans des fichiers XML et obtenir les visualisations sous format de HTML
- observer et explorer les visualisations
  
---

### 19/05/2023
**Perfectionnement des analyses**  
Après avoir consulté le résultat des premiers tests, j'ai remarqué qu'il y avait deux insuffisances :
- le POS-tagging n'est pas correct pour des ponctuations et certains mots vides tels que `d'` `l'`
- les visualisations sont pleines de mots vides et ponctuations, si l'on garde tous types de tokens
- certains types de tokens ne contribuent pas grand chose à l'analyse finale

Ainsi, j'ai décidé de faire quelques modifications dans les scripts, notamment `analyse_sp.py` et `run_lda_filtered.py`. Plus précisément : 
1. Dans `analyse_sp.py`, j'ai rajouté une condition `if` pour effacer dans les exports les ponctuations, les espaces, les mots vides, les nombres (en chiffres et en lettres) et plusieurs POS peu significatifs.
2. Dans `run_lda_filtered.py`, j'ai rajouté une condition `if` pour ne choisir que les noms `NOUN` et les noms propres `PROPN`.

**Constitution et propriétés du corpus**

1. Objectif : Observer les principaux groupes thématiques et la tendance le long de l'année 2022
2. Catégorie : `Europe`
3. Période : 4 sous-corpus, en fonction des 4 trimestres calendaires de l'année, soit :
   - 1e trimestre : 2022-01-01 --> 2022-03-31
   - 2e trimestre : 2022-04-01 --> 2022-06-30
   - 3e trimestre : 2022-07-01 --> 2022-09-30
   - 4e trimestre : 2022-10-01 --> 2022-12-31
4. Nombre de topics pour la visualisation : 10 topics pour chacun des sous-corpus

---

### 20-21/05/2023
**Présentation des résultats**
1. Créer les 4 sous-corpus en fichiers XML avec les scripts modifiés
2. Créer les 4 rapports de visualisation en format HTML avec ces sous-corpus
3. Établir le fichier `index.html` et remplir le contenu du projet dessus
4. Observer les sujets révélés dans les rapports et écrire les analyses thématiques 
